/* copyright

Janienimi - A basic Gemini client
Written in 2022 by Usawashi <usawashi16@yahoo.co.jp>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

copyright */

import javax.swing.JFrame;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.BorderFactory;
import javax.swing.border.Border;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.text.Element;
import javax.swing.text.AttributeSet;
import javax.swing.text.html.HTML;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.HyperlinkEvent;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.X509TrustManager;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.Principal;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateExpiredException;
import java.security.cert.CertificateNotYetValidException;

import java.net.Socket;
import java.net.UnknownHostException;
import java.io.Reader;
import java.io.Writer;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;

class
Janienimi extends JFrame
implements ActionListener, HyperlinkListener {

    private JEditorPane
    document;

    private JLabel
    statusbar;

    private JTextField
    urifield;

    private String
    responseHeader, statusCode, meta;

    private BufferedReader
    responseBody;

    private SSLSocketFactory
    socketFactory;

    private TOFUTrustManager
    trustManager;

//   -  -%-  -

    private final String
    APPNAME = getClass().getName();

//  ---%-@-%---

    public void
    navigateTo(String suri)
    {
        try
        {
            setTitle(suri + " - " + APPNAME);
            status("Navigating to " + suri + "..");
            attemptNavigateTo(suri);
        }
        catch (InputNeededException eIn)
        {
            // Use JOptionPane or smtg, then navigateTo(meta)
        }
        catch (FailureResponseException eFr)
        {
            showErrorPage();
            return;
        }
        catch (UnfamilliarContentTypeException eUct)
        {
            statusCode = "-53";
            meta = "Cannot render " + meta;
            showErrorPage();
            return;
        }
        catch (NonGeminiURIException eIu)
        {
            statusCode = "-52";
            meta = "Tried to navigate to non-Gemini URI";
            showErrorPage();
            return;
        }
        catch (NonGeminiResponseException eNg)
        {
            statusCode = "-51";
            meta = "Server response was not a valid Gemini one.";
            // showErrorPage is supposed to make this description,
            // meta for this will be null later.
            showErrorPage();
            return;
        }
        catch (URISyntaxException eIa)
        {
            statusCode = "-52";
            meta = eIa.getMessage();
            showErrorPage();
            return;
        }
        catch (UnknownHostException eUh)
        {
            statusCode = "-41";
            meta = eUh.getMessage();
            showErrorPage();
            return;
        }
        catch (IOException eIo)
        {
            statusCode = "-50";
            meta = eIo.getMessage();
            showErrorPage();
            return;
        }
    }

//   -  -%-  -

    private void
    status(String message)
    {
        statusbar.setText(message);
        forceRepaint();
    }

    private void
    forceRepaint()
    {
        JPanel c = (JPanel)getContentPane();
        c.paintImmediately(c.getBounds());
    }

    private void
    attemptNavigateTo(String suri)
    throws
        InputNeededException, FailureResponseException,
        UnfamilliarContentTypeException,
        NonGeminiURIException, URISyntaxException,
        UnknownHostException,
        NonGeminiResponseException, IOException
    {
        responseHeader = null;
        statusCode = null;
        meta = null;
        responseBody = null;

        Writer w = null; Reader r = null; try
        {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));

            suri = fixURIString(suri);
            URI uri = new URI(suri);
            urifield.setText(suri);
            forceRepaint();

            boolean foreign = !uri.getScheme().equals("gemini");
            if (foreign) throw new NonGeminiURIException();

            SSLSocket s = getSSLSocket(uri.getHost(), 1965);

            w = new OutputStreamWriter(s.getOutputStream());
            w.write(uri.toString());
            w.write("\r\n");
            w.flush();

            r = new InputStreamReader(s.getInputStream());
            BufferedReader br = new BufferedReader(r);

            responseHeader = br.readLine();
            int o = responseHeader.indexOf(' ');
            if (o == -1) throw new NonGeminiResponseException();

            statusCode = responseHeader.substring(0, o);
            meta = responseHeader.substring(o + 1);

            boolean redirect = statusCode.startsWith("3");
            if (redirect)
            {
                status("Redirecting to " + meta + "..");
                attemptNavigateTo(meta);
                return;
            }

            boolean nonSucc = !statusCode.startsWith("2");
            if (nonSucc) throw new FailureResponseException();

			String[] mime = meta.split(";");
			boolean ctok = meta.isEmpty();
            ctok |= mime[0].equals("text/gemini");
            if (!ctok) throw new UnfamilliarContentTypeException();

			// There could be charset and lang qualifiers to
			// the MIME type. Language we can ignore, but
			// the charset, is it not relevant to how we render?

            responseBody = br;
            parseGeminiText();
            status("Arrived at " + suri + ".");
            document.setCaretPosition(0);
        }
        finally
        {
            if (w != null) w.close();
            if (r != null) r.close();
            setCursor(null);
        }
    }

//   -  -%-  -

    private String
    fixURIString(String suri)
    throws URISyntaxException
    {
        URI uri = new URI(suri);
        String scheme = uri.getScheme();
        String path = uri.getRawPath();

        if (scheme == null) suri = "gemini://" + suri;
        if (!path.endsWith("/")) suri += "/";
        return suri;
    }

    private void
    showErrorPage()
    {
        StringBuilder html = new StringBuilder();
        html.append("<h1>" + statusCode + "</h1>");
        html.append("<span>" + meta + "</span>");
        // An unintelligent error page for now..
        document.setText(html.toString());
    }

    private void
    parseGeminiText()
    throws IOException
    {
        BufferedReader r = responseBody;
        StringBuilder html = new StringBuilder();
        boolean pre = false;
        String line; while ((line = r.readLine()) != null)
        {
            if (line.startsWith("```"))
            {
                pre = !pre;
                html.append(!pre ? "</pre>" : "<pre>");
                continue;
            }

            if (line.startsWith("# "))
            {
                html.append("<h1>");
                html.append(line.substring(2));
                html.append("</h1>");
                continue;
            }

            if (line.startsWith("## "))
            {
                html.append("<h2>");
                html.append(line.substring(3));
                html.append("</h2>");
                continue;
            }

            if (line.startsWith("### "))
            {
                html.append("<h3>");
                html.append(line.substring(4));
                html.append("</h3>");
                continue;
            }

            if (line.startsWith("=>"))
            {
                line = line.replaceFirst("\\s", "\031");
                line = line.replaceFirst("\\s", "\031");
                int o1 = line.indexOf('\031', 0);
                if (o1 == -1) continue;
                int o2 = line.indexOf('\031', o1 + 1);

                String link, text;
                if (o2 == -1)
                {
                    link = line.substring(o1 + 1);
                    text = link;
                }
                else
                {
                    link = line.substring(o1 + 1, o2);
                    text = line.substring(o2 + 1);
                }

                try
                {
                    URI uri = new URI(link);
                    if (uri.getScheme() == null)
                    {
                        String suri = urifield.getText();
                        link = suri + link;
                    }
                }
                catch (URISyntaxException eUs)
                {
                    System.err.println(eUs.getMessage());
                    link = null;
                }

                html.append("<a ");
                if (link != null) html.append("href=" + link);
                html.append(">");
                html.append(text);
                html.append("</a><br>");
                continue;
            }

            html.append(line);
            html.append(pre ? "\n" : "<br>");
        }
        document.setText(html.toString());
    }

    public void
    actionPerformed(ActionEvent eA)
    {
        Object src = eA.getSource();

        if (src == urifield)
        {
            navigateTo(urifield.getText());
        }
    }

    public void
    hyperlinkUpdate(HyperlinkEvent eH)
    {
        HyperlinkEvent.EventType t = eH.getEventType();
        if (t != HyperlinkEvent.EventType.ACTIVATED) return;

        Element e = eH.getSourceElement();
        AttributeSet textElemAttrs = e.getAttributes();
        Object k1 = HTML.Tag.A;
        Object v1 = textElemAttrs.getAttribute(k1);
        AttributeSet aTagAttrs = (AttributeSet)v1;
        Object k2 = HTML.Attribute.HREF;
        Object v2 = aTagAttrs.getAttribute(k2);
        // I rate this API a 5/10, in that it works
        // in the first place, and has some symmetry.

        navigateTo((String)v2);
    }

    private SSLSocket
    getSSLSocket(String host, int port)
    throws UnknownHostException, IOException
    {
        SSLSocket s = (SSLSocket)
            socketFactory.createSocket
                (new Socket(host, port), null, true);

        s.setUseClientMode(true);
        return s;
    }

//  ---%-@-%---

    private class
    TOFUTrustManager
    implements X509TrustManager {

        private KeyStore
        keyStore;

        private File
        keyStoreFile;

//      -=%=-

        public void
        checkServerTrusted(X509Certificate[] chain, String authType)
        throws CertificateException, IllegalArgumentException
        {
            barkAtIllegalArguments(chain, authType);

            X509Certificate currCert, cachedCert;
            byte[] currTBS, cachedTBS;
            String alias;

            currCert = chain[0];
            currTBS = currCert.getTBSCertificate();
            alias = currCert.getSubjectDN().getName();
            // (悪) I have to trust that the subject field
            // matches the host we are trying to connect to..

            try
            {
                cachedCert = (X509Certificate)
                    keyStore.getCertificate(alias);

                if (cachedCert != null)
                    cachedCert.checkValidity();
            }
            catch (CertificateExpiredException eEx)
            {
                cachedCert = null;
            }
            catch (CertificateNotYetValidException eNyv)
            {
                cachedCert = null;
            }
            catch (KeyStoreException eKs)
            {
                cachedCert = null;
                eKs.printStackTrace();
                assert false;
            }

            boolean cached = cachedCert != null;
            boolean mismatch = false;
            if (cached)
            {
                cachedTBS = cachedCert.getTBSCertificate();
                mismatch = !bytewiseEquals(currTBS, cachedTBS);
            }

            if (mismatch || !cached)
            {
                StringBuilder b = new StringBuilder();
                b.append("The certificate given by the server");
                b.append("\ndoes not match the one in our cache.");
                b.append("\nWould you like to trust it?\n");
                b.append("\nIssuer: ");
                b.append(currCert.getIssuerDN().getName());
                b.append("\nSubject: ");
                b.append(currCert.getSubjectDN().getName());
                b.append("\nUntil: ");
                b.append(currCert.getNotAfter().toString());
                b.append("\nStarting: ");
                b.append(currCert.getNotBefore().toString());
                b.append("\nAlgorithm: ");
                b.append(currCert.getSigAlgName());

                int response = JOptionPane.showConfirmDialog(
                    Janienimi.this, b.toString(),
                    "Mismatched certificate",
                    JOptionPane.YES_NO_OPTION
                );
                if (response == JOptionPane.NO_OPTION)
                    throw new CertificateException
                        ("Certificate did not match one in cache");
            }

            try
            {
                keyStore.setCertificateEntry(alias, currCert);
                saveKeyStore();
                return;
            }
            catch (IOException eIo)
            {
                // Leave it unsaved for now.
                eIo.printStackTrace();
                return;
            }
            catch (KeyStoreException eKs)
            {
                // Same.
                eKs.printStackTrace();
                assert false;
                return;
            }
        }

        public X509Certificate[]
        getAcceptedIssuers()
        {
            return new X509Certificate[0];
            // We'll just ignore centralised certificate
            // authorities entirely.
        }

        public void
        checkClientTrusted(X509Certificate[] chain, String authType)
        throws CertificateException, IllegalArgumentException
        {
            String S1;
            S1 = "SSL clients not supported by trust manager";

            barkAtIllegalArguments(chain, authType);
            throw new CertificateException(S1);
        }

//      -=%=-

        private void
        barkAtIllegalArguments(X509Certificate[] c, String at)
        throws IllegalArgumentException
        {
            String S1, S2;
            S1 = "Need non-empty certificate chain!!";
            S2 = "Need non-empty authentication type!";

            if (c == null || c.length == 0)
                throw new IllegalArgumentException(S1);

            if (at == null || at.isEmpty())
                throw new IllegalArgumentException(S2);
        }

        private boolean
        bytewiseEquals(byte[] a1, byte[] a2)
        {
            if (a1.length != a2.length) return false;
            for (int o = 0; o < a1.length; ++o)
                if (a1[o] != a2[o]) return false;
            return true;
        }

//      -=%=-

        private void
        saveKeyStore()
        throws IOException
        {
            assert keyStore != null;
            assert keyStoreFile != null;
            assert keyStoreFile.exists();

            OutputStream os = new FileOutputStream(keyStoreFile);
            try (os)
            {
                keyStore.store(os, new char[0]);
            }
            catch (KeyStoreException eKs)
            {
                eKs.printStackTrace();
                assert false;
            }
            catch (CertificateException eCe)
            {
                eCe.printStackTrace();
                assert false;
            }
            catch (NoSuchAlgorithmException eNsa)
            {
                eNsa.printStackTrace();
                assert false;
            }
        }

        private void
        loadKeyStore()
        throws IOException
        {
            assert keyStoreFile != null;

            try
            {
                keyStore = KeyStore.getInstance("pkcs12");

                if (!keyStoreFile.exists())
                {
                    keyStoreFile.createNewFile();
                    keyStore.load(null, new char[0]);
                    saveKeyStore();
                }

                InputStream is = new FileInputStream(keyStoreFile);
                try (is)
                {
                    keyStore.load(is, new char[0]);
                }
            }
            catch (KeyStoreException eKs)
            {
                eKs.printStackTrace();
                assert false;
            }
            catch (CertificateException eCe)
            {
                eCe.printStackTrace();
                assert false;
            }
            catch (NoSuchAlgorithmException eNsa)
            {
                eNsa.printStackTrace();
                assert false;
            }
        }

    }

//   -  -%-  -

    private static class
    InputNeededException extends Exception { }

    private static class
    FailureResponseException extends Exception { }

    private static class
    NonGeminiURIException extends Exception { }

    private static class
    NonGeminiResponseException extends Exception { }

    private static class
    UnfamilliarContentTypeException extends Exception { }

//  ---%-@-%---

    public static void
    main(String... args)
    {
        new Janienimi().setVisible(true);
    }

//  ---%-@-%---

    Janienimi()
    {
        setTitle(APPNAME);
        initTrustManager();
        initSocketFactory();

        Border b1 = BorderFactory.createEmptyBorder(4, 4, 4, 4);
        Border b2 = BorderFactory.createLineBorder(Color.LIGHT_GRAY);
        Border b3 = BorderFactory.createLoweredBevelBorder();

        Border bc1 = BorderFactory.createCompoundBorder(b2, b1);
        Border bc2 = BorderFactory.createCompoundBorder(b3, b1);

        ((JPanel)getContentPane()).setBorder(b1);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        urifield = new JTextField();
        urifield.setBorder(bc2);
        urifield.addActionListener(this);

        JLabel urifieldlabel = new JLabel("URL:");
        urifieldlabel.setLabelFor(urifield);

        JPanel top = new JPanel();
        top.setBorder(b1);
        top.setLayout(new BorderLayout(4, 0));
        top.add(urifieldlabel, BorderLayout.WEST);
        top.add(urifield);

        statusbar = new JLabel();
        statusbar.setBorder(bc1);

        document = new JEditorPane();
        document.addHyperlinkListener(this);
        document.setBackground(null);
        document.setContentType("text/html");
        document.setEditable(false);

        JScrollPane scroll = new JScrollPane(document);
        scroll.setBorder(null);

        setLayout(new BorderLayout(0, 4));
        add(top, BorderLayout.NORTH);
        add(scroll, BorderLayout.CENTER);
        add(statusbar, BorderLayout.SOUTH);

        setSize(640, 480);
        setLocationByPlatform(true);
        status("Client started.");
    }

    private void
    initTrustManager()
    {
        String s1 = System.getProperty("user.home");
        String s2 = "/.config/Janienimi";
        String s3 = "/keystore";

        try {
            trustManager = new TOFUTrustManager();

            File cf = new File(s1 + s2);
            File kf = new File(s1 + s2 + s3);
            cf.mkdirs();

            trustManager.keyStoreFile = kf;
            trustManager.loadKeyStore();
        }
        catch (IOException eIo) {
            eIo.printStackTrace();
            assert false;
        }
    }

    private void
    initSocketFactory()
    {
        try {
            SSLContext ctx = SSLContext.getInstance("TLSv1.2");
            ctx.init(
                null,
                new X509TrustManager[] { trustManager },
                null
            );
            socketFactory = ctx.getSocketFactory();
        }
        catch (KeyManagementException eKm) {
            eKm.printStackTrace();
            assert false;
        }
        catch (NoSuchAlgorithmException eNsa) {
            eNsa.printStackTrace();
            assert false;
        }
    }

}
